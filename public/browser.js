// create feature
let createField = document.getElementById("create-field")
let listItems = document.getElementById("list-items")

function createItemForm(item) {
    return `
    <li class="list-group-item list-group-item-action d-flex align-items-center justify-content-between">
        <span class="item-text">${item.text}</span>
        <div data-id="${item._id}">
        <button class="edit-me btn btn-secondary btn-sm mr-1">Edit</button>
        <button class="delete-me btn btn-danger btn-sm">Delete</button>
        </div>
    </li>
    `
}

listItems.insertAdjacentHTML("beforeend", itemsInit.map(item => createItemForm(item)).join(''))

document.getElementById("create-form").addEventListener("submit", (e) => {
    e.preventDefault()
    axios.post("/create-item", {item: createField.value}).then((res) => {
        listItems.insertAdjacentHTML("beforeend", createItemForm(res.data))
        createField.value = ""
        createField.focus()
    }).catch(() => {
        alert("add fail, try again")
    })
})

document.addEventListener('click', (e) => {
    if (e.target.classList.contains("edit-me")) {
        let userInput = prompt("Enter new item name", e.target.parentElement.parentElement.querySelector(".item-text").innerHTML)
        if (userInput && userInput != e.target.parentElement.parentElement.querySelector(".item-text").innerHTML) {
            axios.post('/update-item', {id: e.target.parentElement.getAttribute("data-id"), text: userInput}).then(() => {
                e.target.parentElement.parentElement.querySelector(".item-text").innerHTML = res.data.text
            }).catch(() => {
                alert("try update agian!")
            })
        }
    } else if (e.target.classList.contains("delete-me")) {
        if (confirm("delete " + e.target.parentElement.parentElement.querySelector(".item-text").innerHTML)) {
            axios.post('/delete-item', {id: e.target.parentElement.getAttribute("data-id")}).then(() => {
                e.target.parentElement.parentElement.remove()
            }).catch(() => {
                alert("try delete agian!")
            })
        } else {
            
        }
    }
})